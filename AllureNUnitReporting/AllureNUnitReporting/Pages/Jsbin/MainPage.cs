﻿using OpenQA.Selenium;
using AllureNUnitReporting.Browsers;

namespace AllureNUnitReporting.Pages.Jsbin
{
    class MainPage : BasePage
    {
        public MainPage(Browser browser) : base(browser) { }

        #region Elements

        public IWebElement ActiveLine => Browser.FindElement(By.XPath("//div[@class='CodeMirror-activeline']"));
        public IWebElement OuterFrame => Browser.FindElement(By.XPath("//div[@id='live']/iframe"));
        public IWebElement InnerFrame => Browser.FindElement(By.XPath("//div[@id='sandbox-wrapper']/iframe"));
        public IWebElement TestInput => Browser.FindElement(By.XPath("//input[@id='test']"));

        #endregion

        #region Methods

        public void SendTextInBody(string text)
        {
            ActiveLine.Click();
            Browser.SendKeys(text);
        }

        public void SwitchToIFrame(IWebElement frame) => Browser.SwitchToIFrame(frame);

        public void SendKeys(IWebElement toElement, string text) => Browser.SendKeys(toElement, text);

        #endregion
    }
}
