﻿using OpenQA.Selenium;
using AllureNUnitReporting.Browsers;


namespace AllureNUnitReporting.Pages.Toolsqa
{
    class AlertsPage : BasePage
    {
        public AlertsPage(Browser browser) : base(browser) { }

        #region Elements

        public IWebElement SimpleAlertButton => Browser.FindElement(By.XPath("//button[text()='Simple Alert']"));
        public IWebElement ConfirmAlertButton => Browser.FindElement(By.XPath("//button[text()='Confirm Pop up']"));
        public IWebElement PromptAlertButton => Browser.FindElement(By.XPath("//button[text()='Prompt Pop up']"));
        public IWebElement AcceptCookieButton => Browser.FindElement(By.Id("cookie_action_close_header"));

        #endregion

        #region Methods

        public void AcceptCookie()
        {
            try
            {
                Browser.Click(AcceptCookieButton);
            }
            catch (ElementNotInteractableException)
            {
                //Already accepted
            }
        }

        public IAlert GetAlert() => Browser.SwitchToAlert();

        #endregion
    }
}
