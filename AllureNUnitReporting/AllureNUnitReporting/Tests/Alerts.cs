using Allure.Commons;
using AllureNUnitReporting.Browsers;
using AllureNUnitReporting.Pages.Toolsqa;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;

namespace AllureNUnitReporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    public class Alerts
    {
        private readonly string AlertsPageUrl = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
        private readonly string TextToSendInPromptAlert = "Great site";
        private readonly string SimpleAlertText = "A simple Alert";
        private readonly string ConfirmAlertText = "Confirm pop up with OK and Cancel button";
        private readonly string PromptAlertText = "Do NOT you like toolsqa?";
        private Browser Browser { get; set; }
        private AlertsPage AlertsPage { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Browser = Browser.GetInstance();
            Browser.OpenURL(AlertsPageUrl);
            AlertsPage = new AlertsPage(Browser);
            AlertsPage.AcceptCookie();
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Browser.Quit();
        }

        [Test(Description = "Simple Alert Text Test")]
        [AllureTag("SimpleAlert","Regression")]
        [AllureSeverity(SeverityLevel.normal)]
        [AllureIssue("Issue#1")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Alerts")]

        public void SimpleAlertTest()
        {
            AlertsPage.SimpleAlertButton.Click();
            Assert.AreEqual(SimpleAlertText, AlertsPage.GetAlert().Text);
            AlertsPage.GetAlert().Accept();
        }

        [Test(Description = "Confirm Alert Text Test")]
        [AllureTag("ConfirmAlert", "Regression")]
        [AllureSeverity(SeverityLevel.normal)]
        [AllureIssue("Issue#2")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Alerts")]
        public void ConfirmAlertTest()
        {
            AlertsPage.ConfirmAlertButton.Click();
            Assert.AreEqual(ConfirmAlertText, AlertsPage.GetAlert().Text);
            AlertsPage.GetAlert().Accept();
        }

        [Test(Description = "Prompt Alert Text Test")]
        [AllureTag("PromptAlert", "Regression", "Fail")]
        [AllureSeverity(SeverityLevel.normal)]
        [AllureIssue("Issue#3")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Alerts")]
        public void PromptAlertFailTest()
        {
            AlertsPage.PromptAlertButton.Click();
            Assert.AreEqual(PromptAlertText, AlertsPage.GetAlert().Text);
            AlertsPage.GetAlert().SendKeys(TextToSendInPromptAlert);
            AlertsPage.GetAlert().Accept();
        }
    }
}
