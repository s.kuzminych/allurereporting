﻿using Allure.Commons;
using AllureNUnitReporting.Browsers;
using AllureNUnitReporting.Pages.Jsbin;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;

namespace AllureNUnitReporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    public class Frames
    {
        private readonly string JSBinPageUrl = "http://jsbin.com/?html,output";
        private readonly string HTMLToSendInBody = "<input id='test'/>";
        private readonly string TextToSendInTestInput = "любой произвольный текст";
        private Browser Browser { get; set; }
        private MainPage MainPage { get; set; }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Browser = Browser.GetInstance();
            Browser.OpenURL(JSBinPageUrl);
            MainPage = new MainPage(Browser);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Browser.Quit();
        }
        
        [Test(Description = "Switching Frames Test")]
        [AllureTag("Frames")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("Issue-F#B4")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("JSBin")]
        [AllureSubSuite("Frames")]
        public void FramesTest()
        {
            MainPage.SendTextInBody(HTMLToSendInBody);
            MainPage.SwitchToIFrame(MainPage.OuterFrame);
            MainPage.SwitchToIFrame(MainPage.InnerFrame);
            MainPage.SendKeys(MainPage.TestInput, TextToSendInTestInput);
        }
    }
}
