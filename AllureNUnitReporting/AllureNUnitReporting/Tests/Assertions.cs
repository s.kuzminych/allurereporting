﻿using Allure.Commons;
using AllureNUnitReporting.Browsers;
using AllureNUnitReporting.Pages.Toolsqa;
using NUnit.Allure.Attributes;
using NUnit.Allure.Core;
using NUnit.Framework;
using OpenQA.Selenium;
using System;

namespace AllureNUnitReporting.Tests
{
    [TestFixture]
    [AllureNUnit]
    public class Assertions
    {
        private readonly string AlertsPageUrl = "https://www.toolsqa.com/handling-alerts-using-selenium-webdriver/";
        private Browser Browser { get; set; }
        private AlertsPage AlertsPage { get; set; }
        
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Browser = Browser.GetInstance();
            Browser.OpenURL(AlertsPageUrl);
            AlertsPage = new AlertsPage(Browser);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Browser.Quit();
        }

        [Test(Description = "Equality Asserts Using Test")]
        [AllureTag("Asserts", "Equality")]
        [AllureSeverity(SeverityLevel.normal)]
        [AllureIssue("Issue-A#E1")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Asserts")]
        public void EqualityAssertsTest()
        {
            IWebElement originalWebElement = AlertsPage.AcceptCookieButton;
            string acceptCookieButtonText = "ACCEPT";
            Assert.AreEqual(acceptCookieButtonText, originalWebElement.Text);
            acceptCookieButtonText = "Accept";
            Assert.AreNotEqual(acceptCookieButtonText, originalWebElement.Text);
        }

        [Test(Description = "Identity Asserts Using Test")]
        [AllureTag("Asserts", "Identity")]
        [AllureSeverity(SeverityLevel.minor)]
        [AllureIssue("Issue-A#I1")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Asserts")]
        public void IdentityAssertsTest()
        {
            IWebElement originalWebElement = AlertsPage.AcceptCookieButton;
            IWebElement sameWebElement = AlertsPage.AcceptCookieButton;
            Assert.AreNotSame(sameWebElement, originalWebElement);
            IWebElement copiedWebElement = originalWebElement;
            Assert.AreSame(copiedWebElement, originalWebElement);
        }

        [Test(Description = "Condition Asserts Using Test")]
        [AllureTag("Asserts", "Condition")]
        [AllureSeverity(SeverityLevel.minor)]
        [AllureIssue("Issue-C#E1")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Asserts")]
        public void ConditionAssertsTest()
        {
            IWebElement originalWebElement = AlertsPage.AcceptCookieButton;
            string acceptCookieButtonText = "ACCEPT";
            Assert.IsTrue(originalWebElement.Text == acceptCookieButtonText);
            acceptCookieButtonText = "Accept";
            Assert.IsFalse(originalWebElement.Text == acceptCookieButtonText);

            Assert.IsNotEmpty(acceptCookieButtonText);
            acceptCookieButtonText = "";
            Assert.IsEmpty(acceptCookieButtonText);

            Assert.IsNotNull(acceptCookieButtonText);
            acceptCookieButtonText = null;
            Assert.IsNull(acceptCookieButtonText);
        }

        [Test(Description = "Exception Asserts Using Test")]
        [AllureTag("Asserts", "Exception", "Fail")]
        [AllureSeverity(SeverityLevel.critical)]
        [AllureIssue("Issue-E#E1")]
        [AllureOwner("s.kuzminych")]
        [AllureSuite("ToolsQA")]
        [AllureSubSuite("Asserts")]
        public void ExceptionAssertsFailTest()
        {
            Assert.DoesNotThrow(() => DivideByZero());
        }

        private static void DivideByZero()
        {
            throw new DivideByZeroException();
        }
    }
}
