﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AllureNUnitReporting.Browsers
{
    public class Browser
    {
        private IWebDriver Driver { get; }
        public string MainWindowHandle { get; }
        private Browser()
        {
            Driver = new ChromeDriver($"{Environment.CurrentDirectory}\\Resources");
            Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            Driver.Manage().Window.Maximize();
            MainWindowHandle = Driver.CurrentWindowHandle;
        }
        private static Browser Instance;
        public static Browser GetInstance()
        {
            if (Instance == null)
            {
                Instance = new Browser();
            }
            return Instance;
        }

        #region Methods

        public IWebElement FindElement(By by)
        {
            List<IWebElement> element = Instance.Driver.FindElements(by).ToList();
            if (element.Count == 0)
            {
                throw new NoSuchElementException("Cant find the element.");
            }
            else if (element.Count > 1)
            {
                throw new ArgumentOutOfRangeException("Found more than one element.");
            }
            return element[0];
        }
        public List<IWebElement> FindElements(By by)
        {
            List<IWebElement> elements = Instance.Driver.FindElements(by).ToList();
            if (elements.Count == 0)
            {
                throw new NoSuchElementException("There is no such elements");
            }
            return elements;
        }
        public void MoveToElement(IWebElement targetElement)
        {
            new Actions(Driver).MoveToElement(targetElement).Build().Perform();
        }
        //Scroll part (JavaScriptExecutor)
        public void ScrollToTop()
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("window.scrollTo(window.pageXOffset, 0)");
        }
        public void ScrollToBottom()
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("window.scrollTo(window.pageXOffset, document.body.scrollHeight);");
        }

        public void SendKeys(string text)
        {
            new Actions(Driver).SendKeys(text).Build().Perform();
        }
        //Element interaction part (JavaScriptExecutor)
        public void SendKeys(IWebElement element, string text)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].value=arguments[1];", element, text);
        }
        public void Click(IWebElement element)
        {
            ((IJavaScriptExecutor)Driver).ExecuteScript("arguments[0].click();", element);
        }
        public void OpenURL(string URL) => Driver.Navigate().GoToUrl(URL);
        public void Close() => Driver.Close();
        public void Quit()
        {
            Driver.Quit();
            Instance = null;
        }
        public IAlert SwitchToAlert()
        {
            IAlert alert;
            try
            {
                alert = Driver.SwitchTo().Alert();
            }
            catch (NoAlertPresentException)
            {
                alert = SwitchToAlert();
            }
            return alert;
        }
        public void SwitchToWindow(string windowHandle) => Driver.SwitchTo().Window(windowHandle);
        public void SwitchToNextWindow()
        {
            string parentWindowHandle = GetCurrentWindowHandle();
            List<string> windowHandles = GetWindowHandles();
            for (int i = 0; i < windowHandles.Count; i++)
            {
                if (windowHandles[i] == parentWindowHandle)
                {
                    SwitchToWindow(windowHandles[i + 1]);
                    break;
                }
            }
        }
        public void SwitchToIFrame(IWebElement frame)
        {
            MoveToElement(frame);
            Driver.SwitchTo().Frame(frame);
        }
        public string GetCurrentWindowHandle() => Driver.CurrentWindowHandle;
        public List<string> GetWindowHandles() => Driver.WindowHandles.ToList();
        public int GetWindowHandlesCount() => GetWindowHandles().Count;
        public void SwitchToOriginalWindow() => Driver.SwitchTo().Window(MainWindowHandle);
        public void WaitUntilElementIsDisplayed(IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 5));
            wait.Until(Driver => element.Displayed);
        }
        public void WaitUntilElementIsNotDisplayed(IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(Driver, new TimeSpan(0, 0, 5));
            wait.Until(Driver => !(element.Displayed));
        }
        
        #endregion

    }
}
