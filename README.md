# AllureReporting
- This is a simple project that uses Allure as a reporting tool.

***

##### Report generation: #####
1. Install Allure via [Scoop](https://scoop.sh/):
- Open PowerShell
- Run `scoop install allure`
2. Intall with Nuget: 
- Allure.Commons
- AllureCSharpCommons
- NUnit.Allure
3. Create allureConfig.json. My example:
```{
  "allure": {
    "directory": "allure-results",
    "links": [
      "https://example.org/{issue}",
      "https://example.org/{tms}"
    ]
  }
}
```
3. Place `allureConfig.json` in `USER\.nuget\packages\allure.commons\2.4.2.4\lib\netstandard2.0`
4. Run Tests.
5. Generate Report:
- Open `AllureNUnitReporting\AllureNUnitReporting\bin\Debug\netcoreapp2.2` in PowerShell.
- Run `allure generate "allure-results" --clean` where `allure-results` is `directory` value in `allureConfig.json`
- Run `allure open "allure-report"`

***

##### Explanation: #####
- As you can see, test attributes are located before each test method. They provid more detailed information about the method in the report.
- `[Test(Description = "XXX")]`: `Test` is normal NUnit attribute, but now it contains description: more readable name of the method.
- `[AllureTag("Regression")]`: Displays tags that the method matches.
- `[AllureSeverity(SeverityLevel.critical)]`: Displays Severity level: `trivial`, `minor`, `normal`, `critical` or `blocker`.
- `[AllureIssue("ISSUE-1")]`: Displays linked issue. Contains bugID of issue (bug-tracking system).
- `[AllureTms("TMS-12")]`: Displays linked testcase. Contains testID of testcase (test management system).
- `[AllureOwner("User")]`: Displays the author of this method.
- `[AllureSuite("PassedSuite")]`: Displays the suite this method belongs to.
- `[AllureSubSuite("NoAssert")]`: Displays the subsuite this method belongs to.

